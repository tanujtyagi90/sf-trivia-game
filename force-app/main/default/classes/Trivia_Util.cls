/**
*─────────────────────────────────────────────────────────────────────────────────────────────────
* Utility Class 
*
* Additional information - Utility Class to perform common operations i.e - Check FLS/Object Accessibilty
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author Tanuj 
* @date 17/20/2019
* @description  Class called to perform common utility operations
* @modifiedBy     Tanuj
* @maintainedBy   Tanuj
* @version        1.0
* @created        17/20/2019
* @modified       17/20/2019
──────────────────────────────────────────────────────────────────────────────────────────────────
* Modification Log
* ----------------
* Date        Developer     Chg #         Comments
* ----------- ------------- ------------- ------------------------------------------------

*/
public with sharing class Trivia_Util {
    
       /*******************************************************************************************************
    * @description Check FLS Access for Creation of given Fields
    * @param objectName Name of the Object
    * @param FieldsToInsert List of Fields to Insert
    * @return True/False (Whether User has Create Access or Not)
    */
public static boolean getCreateAccessCheck(string objectName, List<string> FieldsToInsert) {
        Boolean permission = false;
        if (Schema.getGlobalDescribe().get(objectName) != null) {
            Map<String, Schema.SObjectField> m = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap();
            permission = true;
            for (String fieldToCheck : FieldsToInsert) {
                system.debug('fieldToCheck : '+fieldToCheck);
                if (m.get(fieldToCheck) != null && (!m.get(fieldToCheck).getDescribe().isCreateable()) && (!m.get(fieldToCheck).getDescribe().isAccessible())) {
                    permission = false;
                }
            }
        }
    system.debug('permission : '+permission);
        return permission;
    }
    
           /*******************************************************************************************************
    * @description Check FLS Access for Update of given Fields
    * @param objectName Name of the Object
    * @param FieldsToUpdate List of Fields to Update
    * @return True/False (Whether User has Update Access or Not)
    */

    public static boolean getUpdateAccessCheck(string objectName, List<string> FieldsToUpdate) {
        Boolean permission = false;
        if (Schema.getGlobalDescribe().get(objectName) != null) {
            Map<String, Schema.SObjectField> m = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap();
            system.debug('mappis--' + m);
            permission = true;
            for (String fieldToCheck : FieldsToUpdate) {
                if (m.get(fieldToCheck) != null && (!m.get(fieldToCheck).getDescribe().isUpdateable()) && (!m.get(fieldToCheck).getDescribe().isAccessible())) {
                    permission = false;
                }
            }
        }
        return permission;
    }
}