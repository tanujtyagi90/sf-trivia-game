({
    onReadyGame: function(component, event, helper) {
        component.set("v.ScreenOne", false);
        // send apex call to create game record and display the game id.
        helper.createGameRecord(component, event, helper);
    },
    
    updateSelectedText: function(cmp, event) {
        var selectedRows = event.getParam('selectedRows');
        cmp.set('v.selectedRowsCount', selectedRows.length);
    },
    

    //For Ban selected records 
    onBanSelected: function(component, event, helper) {
        
        //Get the event listOfIds attribute
        var delId = event.getParam("listOfIds");
        // call the helper function and pass all selected record id's.    
        helper.addSelectedHelper(component, event, delId, helper);
        
    },
    
    onStartGame: function(component, event, helper) {
        component.set('v.stopPolling', false);
        if (component.get("v.participantsList") != null) {
            component.set('v.ScreenOne', false);
            component.set('v.ScreenTwo', false);
            helper.getRecordsFromqAndA(component, event, helper);
            
        } else {
            alert('No Particpants in the Game');
        }
        
    },
    
    onUpdateQandAns: function(component, event, helper) {
        var defaultquesAns = component.get("v.defaultQuestionsInGame");
        if (defaultquesAns != null && (defaultquesAns > 0 && defaultquesAns <= 5)) {
            
            helper.updateQandAnsFromServer(component, event, helper);
        } else {
            alert('Invalid Entry');
        }
        
    },
    
    OnNextQuesAns: function(component, event, helper) {
        if (component.get("v.participantsList") != null) {
            component.set('v.ScreenOne', false);
            component.set('v.ScreenTwo', false);
            var quesAsked = component.get("v.QuestionsAskedInGame");
            
            if (quesAsked == component.get("v.defaultQuestionsInGame")) {
                // call the method for the final Screen Result view    
                helper.callApexforResult(component, event, helper);
                
            } else {
                helper.getRecordsFromqAndA(component, event, helper);
            }
            
        } else {
            alert('No Particpants in the Game');
        }
        
    },
    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true);
    },
    
    // this function automatic call by aura:doneWaiting event 
    hideSpinner: function(component, event, helper) {
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner", false);
    }
    
    
    
})