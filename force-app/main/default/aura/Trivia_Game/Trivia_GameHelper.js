({
    createGameRecord: function(component, event, helper) {
        var action = component.get("c.createGame");
        action.setCallback(this, function(response) {
            //store state of response
            var state = response.getState();
            if (state === "SUCCESS") {
                var resultData = response.getReturnValue();
                component.set("v.Game", resultData);
                component.set("v.defaultQuestionsInGame", resultData.No_of_Questions__c);
                component.set("v.ScreenTwo", true);
                helper.getPartcipantsData(component, event, helper);
            } else if (state === "INCOMPLETE") {
                // do something
            } else if (state === "ERROR") {
                var errors = response.getError();
                // Configure error toast
                let toastParams = {
                    title: "Error",
                    message: "Unknown error", // Default error message
                    type: "error"
                };
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        toastParams.message = errors[0].message;
                        console.log("Error message: " +
                            errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
                // Fire error toast
                let toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams(toastParams);
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
    },

    getPartcipantsData: function(component, event, helper) {

        // make apex call to game records to get participants as they are joining the game   

        helper.callApexMethod(component, event, helper);

        //execute callApexMethod() again after 5 sec each

        window.setInterval(
            $A.getCallback(function() {
                if (component.get("v.stopPolling") == true)
                    helper.callApexMethod(component, helper);
            }), 5000
        );
    },

    handleResponse: function(response, component) {
        var state = response.getState();
        if (state === "SUCCESS") {
            console.log(state);
            var retVal = response.getReturnValue();
            console.log('retVal : '+JSON.stringify(retVal));
            component.set("v.participantsList", retVal);
        }
    },



    addSelectedHelper: function(component, event, deleteRecordsIds, helper) {
        //call apex class method
        var gameId = component.get("v.Game.Name");
        var action = component.get('c.setBanOnParticipants');
        // pass the all selected record's Id's to apex method 
        action.setParams({
            "gameId": gameId,
            "participantIdList": deleteRecordsIds
        });
        action.setCallback(this, function(response) {
            //store state of response
            var state = response.getState();
            if (state === "SUCCESS") {
                if (response.getReturnValue() == null) {
                    // if getting any error while delete the records , then display a alert msg/
                    var showToast = $A.get('e.force:showToast');
                    showToast.setParams({
                        'title': '',
                        'message': 'The following error has occurred. ->' + response.getReturnValue(),
                        'type': 'Error'
                    });
                    showToast.fire();
                } else {
                    var retVal = response.getReturnValue();
                    var showToast = $A.get('e.force:showToast');
                    showToast.setParams({
                        'title': '',
                        'message': 'Record Successfully Banned.',
                        type: 'success'
                    });
                    showToast.fire();
                    component.set("v.selectedCount", 0);
                    // call the onLoad function for refresh the List view    
                    helper.callApexMethod(component, event, helper);
                }
            }
        });
        $A.enqueueAction(action);
    },



    callApexMethod: function(component, event, helper) {
        var gameId = component.get("v.Game.Name");

        if (gameId != null) {
            var action = component.get("c.getParticipantsList");

            action.setParams({
                "gameId": gameId
            });

            action.setCallback(this, function(response) {
                this.handleResponse(response, component);
            });
            $A.enqueueAction(action);
        }

    },

    getRecordsFromqAndA: function(component, event, helper) {
        var gameId = component.get("v.Game.Name");
        if (gameId != null) {
            var action = component.get("c.getandSetQAtoParticipants");

            action.setParams({
                "gameId": gameId,
                "gameparticipantList": component.get("v.participantsList"),
                "askedquestionsSet": component.get("v.questionAndAnsSet")
            });

            action.setCallback(this, function(response) {
                //store state of response
                var state = response.getState();
                if (state === "SUCCESS") {
                    console.log(state);
                    var resultData = response.getReturnValue();
                    component.set("v.qAndARecord", resultData);

                    // creating an empty set 
                    var setOfQues = [];

                    setOfQues = component.get("v.questionAndAnsSet");
                    setOfQues.push(resultData.Id);
                    component.set("v.questionAndAnsSet", setOfQues);
                    component.set("v.ScreenThree", true);
                    var quesAsked = component.get("v.QuestionsAskedInGame");
                    // check, if selected checkbox value is true then increment getSelectedNumber with 1 
                    // else Decrement the getSelectedNumber with 1     
                    quesAsked++;

                    // set the actual value on selectedCount attribute to show on header part. 
                    component.set("v.QuestionsAskedInGame", quesAsked);

                    if (quesAsked == component.get("v.defaultQuestionsInGame")) {
                        component.set("v.buttonLabel", 'End Game');
                    }


                } else if (state === "INCOMPLETE") {
                    // do something
                } else if (state === "ERROR") {
                    var errors = response.getError();
                    // Configure error toast
                    let toastParams = {
                        title: "Error",
                        message: "Unknown error", // Default error message
                        type: "error"
                    };
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            toastParams.message = errors[0].message;
                            console.log("Error message: " +
                                errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                    // Fire error toast
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                }
            });
            $A.enqueueAction(action);
        }

    },

    callApexforResult: function(component, event, helper) {
        var action = component.get("c.getFinalResults");
        action.setParams({
            "gameObj": component.get("v.Game")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(state);
                var resultData = response.getReturnValue();
                component.set("v.finalGameResult", resultData);
                component.set("v.ScreenThree", false);
                component.set("v.ScreenFour", true);
            } else if (state === "INCOMPLETE") {
                // do something
            } else if (state === "ERROR") {
                var errors = response.getError();
                // Configure error toast
                let toastParams = {
                    title: "Error",
                    message: "Unknown error", // Default error message
                    type: "error"
                };
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        toastParams.message = errors[0].message;
                        console.log("Error message: " +
                            errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
                // Fire error toast
                let toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams(toastParams);
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);

    },




    updateQandAnsFromServer: function(component, event, helper) {
        var action = component.get("c.updateQandAnsFromServer");
        action.setParams({
            "gameObj": component.get("v.Game"),
            "noOfQues": component.get("v.defaultQuestionsInGame"),
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var resultData = response.getReturnValue();
            } else if (state === "INCOMPLETE") {
                // do something
            } else if (state === "ERROR") {
                var errors = response.getError();
                // Configure error toast
                let toastParams = {
                    title: "Error",
                    message: "Unknown error", // Default error message
                    type: "error"
                };
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        toastParams.message = errors[0].message;
                        console.log("Error message: " +
                            errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
                // Fire error toast
                let toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams(toastParams);
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
    }

})