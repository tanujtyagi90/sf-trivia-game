({
    nextQuesAns : function(component) {
        var nextQuesAnsEvent = component.getEvent("nextQuesAns");
        nextQuesAnsEvent.fire();
    },
})