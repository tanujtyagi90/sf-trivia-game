({

    MAX_FILE_SIZE: 4500000, //Max file size 4.5 MB 
    CHUNK_SIZE: 750000, //Chunk Max size 750Kb 


    getParticipantRecordFromServer: function(component, event, helper, recordId) {
        console.log('getParticipantRecordFromServer');
        var action = component.get("c.getParticipantRecord");
        action.setParams({
            partcpntRecordId: recordId
        });
        action.setCallback(this, function(response) {
            //store state of response
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(state);
                var resultData = response.getReturnValue();
                console.log('resultData : ' + JSON.stringify(resultData));
                component.set("v.ParticipantRec", resultData);
            } else if (state === "INCOMPLETE") {
                // do something
            } else if (state === "ERROR") {
                var errors = response.getError();
                // Configure error toast

                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                            errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }

            }
        });
        $A.enqueueAction(action);

    },




    createParticipantRecord: function(component, event, helper, recordId) {
        var action = component.get("c.saveParticipant");
        action.setParams({
            partcpntRecord: component.get("v.ParticipantRec")
        });
        action.setCallback(this, function(response) {
            //store state of response
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(state);
                var resultData = response.getReturnValue();
                console.log('resultData : ' + JSON.stringify(resultData));

                if (resultData == 'Success') {
                    alert('Your Details Updated Successfully! . You are in the Game Now!');
                    window.close();

                }




            } else if (state === "INCOMPLETE") {
                // do something
            } else if (state === "ERROR") {
                var errors = response.getError();
                // Configure error toast
                console.log("Unknown error");
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                            errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }

            }
        });
        $A.enqueueAction(action);

    },

    uploadStart: function(component, event, recordId) {
        var fileInput = component.find("fuploader").get("v.files");
        // get the first file using array index[0]  
        var file = fileInput[0];
        var self = this;
        // check the selected file size, if select file size greter then MAX_FILE_SIZE,
        // then show a alert msg to user,hide the loading spinner and return from function  
        if (file.size > self.MAX_FILE_SIZE) {
            component.set("v.fileName", 'Alert : File size cannot exceed ' + self.MAX_FILE_SIZE + ' bytes.\n' + ' Selected file size: ' + file.size);
            return;
        }

        // create a FileReader object 
        var objFileReader = new FileReader();
        // set onload function of FileReader object   
        objFileReader.onload = $A.getCallback(function() {
            var fileContents = objFileReader.result;
            var base64 = 'base64,';
            var dataStart = fileContents.indexOf(base64) + base64.length;

            fileContents = fileContents.substring(dataStart);
            // call the uploadProcess method 
            self.uploadProcess(component, file, fileContents, recordId);
        });

        objFileReader.readAsDataURL(file);
    },


    uploadProcess: function(component, file, fileContents, recordId) {
        // set a default size or startpostiton as 0 
        var startPosition = 0;
        // calculate the end size or endPostion using Math.min() function which is return the min. value   
        var endPosition = Math.min(fileContents.length, startPosition + this.CHUNK_SIZE);

        // start with the initial chunk, and set the attachId(last parameter)is null in begin
        this.uploadInChunk(component, file, fileContents, startPosition, endPosition, '', recordId);
    },


    uploadInChunk: function(component, file, fileContents, startPosition, endPosition, attachId, recordId) {
        // call the apex method 'saveParticipant'
        var getchunk = fileContents.substring(startPosition, endPosition);
        var action = component.get("c.uploadImage");
        console.log('calling');
        action.setParams({
            parentId: component.get("v.partcpntId"),
            fileName: file.name,
            base64Data: encodeURIComponent(getchunk),
            contentType: file.type,
            fileId: attachId
        });
        // set call back 
        action.setCallback(this, function(response) {
            // store the response / Attachment Id   
            attachId = response.getReturnValue();
            var state = response.getState();
            if (state === "SUCCESS") {
                // update the start position with end postion
                startPosition = endPosition;
                endPosition = Math.min(fileContents.length, startPosition + this.CHUNK_SIZE);
                // check if the start postion is still less then end postion 
                // then call again 'uploadInChunk' method , 
                // else, diaply alert msg and hide the loading spinner
                if (startPosition < endPosition) {
                    console.log('calling again');
                    this.uploadInChunk(component, file, fileContents, startPosition, endPosition, attachId, recordId);
                } else {
                    console.log('Participant created successfully');
                    //   alert('Your Details Updated Successfully! . You are in the Game Now!');
                    component.set("v.attachID", attachId);
                    //   component.set("v.showButton",false);
                    component.set("v.showImage", true);
                }
                // handel the response errors        
            } else if (state === "INCOMPLETE") {
                alert("From server: " + response.getReturnValue());
            } else {
                console.log("Failed with state: " + state);
                var errors = action.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("...Error message: .. " + errors[0].message);


                    }
                } else {
                    console.log("Unknown error");

                }
            }
        });
        // enqueue the action
        $A.enqueueAction(action);
    },



})