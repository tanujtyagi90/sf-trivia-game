({
    handleOnload: function(component, event, helper) {
        var recordId = component.get("v.partcpntId");
        if (recordId != null) {
            helper.getParticipantRecordFromServer(component, event, helper, recordId);
        }
    },

    handleOnSubmit: function(component, event, helper) {
        event.preventDefault();
        // //getting the User information
        var ParticipantRec = component.get("v.ParticipantRec");
        var recordId = component.get("v.partcpntId");

        helper.createParticipantRecord(component, event, helper, recordId);




    },

    handleFilesChange: function(component, event, helper) {
        var fileName = 'No File Selected..';
        if (event.getSource().get("v.files").length > 0) {
            fileName = event.getSource().get("v.files")[0]['name'];
            component.set("v.showButton", true);
        }
        component.set("v.fileName", fileName);
    },

    doSave: function(component, event, helper) {
        var recordId = component.get("v.partcpntId");
        if (component.find("fuploader").get("v.files").length > 0) {
            helper.uploadStart(component, event, recordId);
        } else {
            alert('Please Select a Valid File');
        }
    },

    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true);
    },

    // this function automatic call by aura:doneWaiting event 
    hideSpinner: function(component, event, helper) {
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner", false);
    }
})