({
    banSelected : function(component, event, helper) {
       
        
        // create var for store record id's for selected checkboxes  
        var delId = [];
        // get all checkboxes 
        var getAllId = component.find("boxPack");
        // If the local ID is unique[in single record case], find() returns the component. not array
        if (!Array.isArray(getAllId)) {
            if (getAllId.get("v.value") == true) {
                delId.push(getAllId.get("v.text"));
            }
        } else {
            // play a for loop and check every checkbox values 
            // if value is checked(true) then add those Id (store in Text attribute on checkbox) in delId var.
            for (var i = 0; i < getAllId.length; i++) {
                if (getAllId[i].get("v.value") == true) {
                    delId.push(getAllId[i].get("v.text"));
                }
            }
        }
        
        if (delId.length === 0) {
            var showToast = $A.get('e.force:showToast');
            showToast.setParams({
                'title': '',
                'message': 'Please Select any Record to Ban.',
            });
            showToast.fire();
            return false;
        }
        
         var blockUserEvent = component.getEvent("blockUser");
        //Set event attribute value
        blockUserEvent.setParams({"listOfIds" : delId}); 
        
        blockUserEvent.fire();
    },
    
    startGame : function(component, event, helper) {
        var startGameEvent = component.getEvent("startGame");
        startGameEvent.fire();
        
    },
    
    // For count the selected checkboxes. 
    checkboxSelect: function(component, event, helper) {
        component.set('v.stopPolling', false);
        // get the selected checkbox value  
        var selectedRec = event.getSource().get("v.value");
        // get the selectedCount attrbute value(default is 0) for add/less numbers. 
        var getSelectedNumber = component.get("v.selectedCount");
        // check, if selected checkbox value is true then increment getSelectedNumber with 1 
        // else Decrement the getSelectedNumber with 1     
        if (selectedRec == true) {
            getSelectedNumber++;
        } else {
            getSelectedNumber--;
        }
        // set the actual value on selectedCount attribute to show on header part. 
        component.set("v.selectedCount", getSelectedNumber);
    },
})