({
	startGame: function(component, event, helper) {
        var readyGameEvent = component.getEvent("readyGame");
        readyGameEvent.fire();
    },
})